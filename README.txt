Drupal module: Language text filter
===========================

language_text_filter.module is the basic module, offering new filter called 'Language text filter'.
It allows to change links to the translated node, dependence on the curretnt user lang.

Installation:
  Installation is like with all normal drupal modules:

The module currently only supports path prefix.

Configuration:
  The configuration page is at admin/config/content/formats,
  where you can choose one of the Text formats, and to add new filter to it (admin/config/content/formats/full_html),
  enable 'Language text filter' and add Site Url Domain (e.g. peytz.dk).
